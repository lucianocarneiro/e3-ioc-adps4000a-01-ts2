export IOCNAME=TS2-010:Ctrl-IOC-004
export AS_TOP=/opt/nonvolatile
export IOCDIR=TS2-010_Ctrl-IOC-004
export IOCNAME_SLUG=TS2-010_Ctrl-IOC-004
export SETTINGS_FILES=settings
export LOCATION=TS2-010
export ENGINEER="Luciano C Guedes <luciano.carneiroguedes@ess.eu>"
